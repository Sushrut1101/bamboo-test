wget https://github.com/owenthereal/upterm/releases/download/v0.10.0/upterm_linux_amd64.tar.gz

mkdir -p upterm
cd upterm
tar -xf upterm*
mv upterm /usr/local/bin/
cd -

upterm host -- ${SHELL}
